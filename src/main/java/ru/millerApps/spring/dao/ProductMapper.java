package ru.millerApps.spring.dao;

import org.springframework.jdbc.core.RowMapper;
import ru.millerApps.spring.models.Product;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProductMapper implements RowMapper<Product> {
    @Override
    public Product mapRow(ResultSet resultSet, int i) throws SQLException {
        Product product = new Product();

        product.setId(resultSet.getInt("id"));
        product.setName(resultSet.getString("name"));
        product.setQuantity(resultSet.getInt("quantity"));
        product.setMass(resultSet.getInt("mass"));
        return product;
    }
}