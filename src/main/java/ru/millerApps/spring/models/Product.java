package ru.millerApps.spring.models;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Product {
    private int id;

    @NotEmpty(message = "Name should not be empty")
    @Size(min = 2, max = 30, message = "Name should be between 2 and 30 characters")
    private String name;

    @Min(value = 0, message = "Can not be less than 0")
    @NotNull(message = "Input how much product it is")
    private int quantity;

    @Min(value = 0, message = "Can not be less than 0")
    private int mass;


    public Product(int id, String name,int quantity, int mass){
        this.id = id;
        this.name = name;
        this.quantity = quantity;
        this.mass = mass;
    }

    public Product() {}

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getMass() {
        return mass;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setMass(int mass) {
        this.mass = mass;
    }
}
