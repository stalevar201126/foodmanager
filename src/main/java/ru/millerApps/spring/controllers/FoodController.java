package ru.millerApps.spring.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.millerApps.spring.dao.ProductDAO;
import ru.millerApps.spring.models.Product;

import javax.validation.Valid;


@Controller
@RequestMapping("/food")
public class FoodController {

    private final ProductDAO productDAO;

    @Autowired
    public FoodController(ProductDAO productDAO) {
        this.productDAO = productDAO;
    }

    @GetMapping()
    public String index(Model model){
        //получим всю еду из ДАО и передадим на отображение в представлении
        model.addAttribute("food", productDAO.index());
        return "food/index";
    }

    @GetMapping("/{id}")
    public String show(@PathVariable("id") int id,
                       Model model){
        model.addAttribute("product", productDAO.show(id));
        return "food/show";
    }

    @GetMapping("/new")
    public String newProduct(Model model){
        model.addAttribute("product", new Product());
        return "food/new";
    }

    @PostMapping()
    public String create(@ModelAttribute("product") @Valid Product product, BindingResult bindingResult){
        if (bindingResult.hasErrors())
            return "food/new";

        productDAO.save(product);
        return "redirect:/food";
    }

    @GetMapping("/{id}/edit")
    public String edit(Model model, @PathVariable("id") int id){
        model.addAttribute("product", productDAO.show(id));
        return "food/edit";
    }

    @PatchMapping("/{id}")
    public String update(@ModelAttribute("product") @Valid Product product, BindingResult bindingResult,
                         @PathVariable("id") int id){
        if(bindingResult.hasErrors())
            return "food/edit";
        productDAO.update(id, product);
        return "redirect:/food";
    }

    @DeleteMapping("/{id}")
    public String delete(@PathVariable("id") int id){
        productDAO.delete(id);
        return "redirect:/food";
    }
}
